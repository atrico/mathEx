package mathEx

// Generate all primes less than max
func GeneratePrimes(max uint) (primes []uint) {
	if max > 1 {
		primes = make([]uint, 0, max)
		// Sieve
		sieve := make([]bool, max)
		sieve[0] = true
		sieve[1] = true
		current := uint(2)
		for current < max {
			// Find next prime
			if !sieve[current] {
				primes = append(primes, current)
				// Sieve this prime
				if current <= max/2 {
					for i := current * 2; i < max; i += current {
						sieve[i] = true
					}
				}
			}
			current++
		}
	}
	return
}

func CalculatePrimeFactors(value uint) (factors []ValueWithExponent) {
	primes := GeneratePrimes(value + 1)
	for _, p := range primes {
		// Check for done
		if value < 2 {
			break
		}
		// Is this a factor?
		fCount := uint(0)
		for value > 0 && value%p == 0 {
			fCount++
			value /= p
		}
		if fCount > 0 {
			factors = append(factors, ValueWithExponent{Value: p, Exponent: fCount})
		}
	}
	return
}
