package mathEx

import "math/big"

func Factorial(val uint) (result *big.Int) {
	if val == 0 {
		return big.NewInt(1)
	}
	return FactorialDiv(val, 1)
}

func FactorialDiv(top, bottom uint) (result *big.Int) {
	if top < bottom {
		panic("numerator must be >= denominator")
	}
	result = big.NewInt(1)
	for i := int64(bottom + 1); i <= int64(top); i++ {
		result.Mul(result, big.NewInt(i))
	}
	return result
}
