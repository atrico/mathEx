package mathEx

import (
	"fmt"
	"math"
)

type ValueWithExponent struct {
	Value, Exponent uint
}

func (v ValueWithExponent) Calculate() uint {
	return uint(math.Pow(float64(v.Value), float64(v.Exponent)))
}

func (v ValueWithExponent) String() string {
	return fmt.Sprintf("%d^%d", v.Value, v.Exponent)
}
