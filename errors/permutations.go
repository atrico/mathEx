package errors

// ----------------------------------------------------------------------------------------------------------------------------
// No members in population
// ----------------------------------------------------------------------------------------------------------------------------

type EmptyPopulation struct{}

func (e EmptyPopulation) Error() string {
	return "Population is empty"
}

// ----------------------------------------------------------------------------------------------------------------------------
// Sample set larger than population population
// ----------------------------------------------------------------------------------------------------------------------------

type SampleSetTooBig struct{}

func (e SampleSetTooBig) Error() string {
	return "Sample set is larger than the population"
}
