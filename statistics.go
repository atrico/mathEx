package mathEx

import (
	"gitlab.com/atrico/core/v2"
	"reflect"
	"sort"

	"gitlab.com/atrico/mathEx/v2/constraints"
)

type PopulationComplex[T constraints.NumericAndComplex] interface {
	// Number of values in population
	Count() int
	// The value
	// Order is not guaranteed and may change between calls
	Values() []T
	// All the values summed together
	Sum() T
	// Mean value
	Mean() T
	// Variance
	Variance() T
	// Standard deviation
	StandardDeviation() T
}
type Population[T constraints.Numeric] interface {
	PopulationComplex[T]
	// Median value
	Median() T
	// Gini Impurity
	GiniImpurity() float64
}

type MutablePopulationComplex[T constraints.NumericAndComplex] interface {
	PopulationComplex[T]
	AddValue(value T) MutablePopulationComplex[T]
}

type MutablePopulation[T constraints.Numeric] interface {
	Population[T]
	AddValue(value T) MutablePopulation[T]
}

func MakePopulation[T constraints.Numeric](values ...T) Population[T] {
	pop := makePopulation[T](values)
	return pop
}
func MakePopulationComplex[T constraints.NumericAndComplex](values ...T) PopulationComplex[T] {
	pop := makePopulation[T](values)
	return pop
}

func MakeMutablePopulation[T constraints.Numeric](values ...T) MutablePopulation[T] {
	pop := mutablePopulation[T]{makePopulation[T](values)}
	return &pop
}
func MakeMutablePopulationComplex[T constraints.NumericAndComplex](values ...T) MutablePopulationComplex[T] {
	pop := mutablePopulationComplex[T]{makePopulation[T](values)}
	return &pop
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------
type population[T constraints.NumericAndComplex] struct {
	values   []T
	sum      core.CachedValue[T]
	mean     core.CachedValue[T]
	median   core.CachedValue[T]
	stdDev   core.CachedValue[T]
	variance core.CachedValue[T]
	gini     core.CachedValue[float64]
}

type mutablePopulation[T constraints.Numeric] struct {
	pop *population[T]
}

type mutablePopulationComplex[T constraints.NumericAndComplex] struct {
	pop *population[T]
}

func makePopulation[T constraints.NumericAndComplex](values []T) *population[T] {
	pop := population[T]{values: values}
	pop.sum = core.NewCachedValue[T](func() T { return Sum(pop.values) })
	pop.mean = core.NewCachedValue[T](func() T { return pop.calcMean() })
	pop.stdDev = core.NewCachedValue[T](func() T { return SqrRoot(pop.Variance()) })
	pop.variance = core.NewCachedValue[T](func() T { return pop.calcVariance() })
	// Numeric only
	pop.median = core.NewCachedValue[T](func() T { return pop.calcMedian() })
	pop.gini = core.NewCachedValue[float64](func() float64 { return pop.calcGiniImpurity() })
	return &pop
}

func (p *population[T]) Count() int {
	return len(p.values)
}

func (p *population[T]) Values() []T {
	return p.values
}

func (p *population[T]) Sum() T {
	return p.sum.GetValue()
}

func (p *population[T]) Mean() T {
	return p.mean.GetValue()
}

func (p *population[T]) StandardDeviation() T {
	return p.stdDev.GetValue()
}

func (p *population[T]) Variance() T {
	return p.variance.GetValue()
}

func (p *population[T]) Median() T {
	return p.median.GetValue()
}

func (p *population[T]) GiniImpurity() float64 {
	return p.gini.GetValue()
}

func (p *population[T]) addValue(value T) {
	p.values = append(p.values, value)
	p.sum.Reset()
	p.mean.Reset()
	p.stdDev.Reset()
	p.variance.Reset()
	p.median.Reset()
	p.gini.Reset()
}

// ----------------------------------------------------------------------------------------------------------------------------
// Mutable
// ----------------------------------------------------------------------------------------------------------------------------
func (p *mutablePopulation[T]) Count() int {
	return p.pop.Count()
}

func (p *mutablePopulation[T]) Values() []T {
	return p.pop.Values()
}

func (p *mutablePopulation[T]) Sum() T {
	return p.pop.Sum()
}

func (p *mutablePopulation[T]) Mean() T {
	return p.pop.Mean()
}

func (p *mutablePopulation[T]) Variance() T {
	return p.pop.Variance()
}

func (p *mutablePopulation[T]) StandardDeviation() T {
	return p.pop.StandardDeviation()
}

func (p *mutablePopulation[T]) Median() T {
	return p.pop.Median()
}

func (p *mutablePopulation[T]) GiniImpurity() float64 {
	return p.pop.GiniImpurity()
}

func (p *mutablePopulation[T]) AddValue(value T) MutablePopulation[T] {
	p.pop.addValue(value)
	return p
}

func (p *mutablePopulationComplex[T]) Count() int {
	return p.pop.Count()
}

func (p *mutablePopulationComplex[T]) Values() []T {
	return p.pop.Values()
}

func (p *mutablePopulationComplex[T]) Sum() T {
	return p.pop.Sum()
}

func (p *mutablePopulationComplex[T]) Mean() T {
	return p.pop.Mean()
}

func (p *mutablePopulationComplex[T]) Variance() T {
	return p.pop.Variance()
}

func (p *mutablePopulationComplex[T]) StandardDeviation() T {
	return p.pop.StandardDeviation()
}

func (p *mutablePopulationComplex[T]) AddValue(value T) MutablePopulationComplex[T] {
	p.pop.addValue(value)
	return p
}

func fromInt[T constraints.NumericAndComplex](val int) (result T) {
	var rValue reflect.Value
	rType := reflect.TypeOf(result)
	switch rType.Kind() {
	case reflect.Complex64:
		rValue = reflect.ValueOf(complex(float32(val), 0))
	case reflect.Complex128:
		rValue = reflect.ValueOf(complex(float64(val), 0))
	default:
		rValue = reflect.ValueOf(val).Convert(rType)
	}
	reflect.ValueOf(&result).Elem().Set(rValue)
	return result
}

func (p *population[T]) calcVariance() (result T) {
	for _, val := range p.values {
		result += (val - p.Mean()) * (val - p.Mean())
	}
	result /= fromInt[T](p.Count())
	return result
}
func (p *population[T]) calcMean() (result T) {
	return p.Sum() / fromInt[T](p.Count())
}

func (p *population[T]) calcMedian() (result T) {
	// Sort the values
	sort.Slice(p.values, func(i, j int) bool { return less(p.values[i], p.values[j]) })
	mid := len(p.values) / 2
	if (len(p.values) & 1) == 1 {
		// Odd length
		result = p.values[mid]
	} else {
		// Even length
		result = (p.values[mid] + p.values[mid-1]) / 2
	}
	return result
}

func (p *population[T]) calcGiniImpurity() (result float64) {
	rType := reflect.TypeOf(result)
	fSum := reflect.ValueOf(p.Sum()).Convert(rType).Float()
	for _, val := range p.values {
		fVal := reflect.ValueOf(val).Convert(rType).Float()
		prob := fVal / fSum
		result += prob * (1 - prob)
	}
	return
}

func less[T constraints.NumericAndComplex](lhs, rhs T) bool {
	rVal := reflect.ValueOf(lhs)
	var lhsF, rhsF float64
	fType := reflect.TypeOf(lhsF)
	if rVal.Kind() == reflect.Complex64 || rVal.Kind() == reflect.Complex128 {
		lhsC := reflect.ValueOf(lhs).Complex()
		rhsC := reflect.ValueOf(rhs).Complex()
		lhsF = real(lhsC)*real(lhsC) + imag(lhsC)*imag(lhsC)
		rhsF = real(rhsC)*real(rhsC) + imag(rhsC)*imag(rhsC)
	} else {
		lhsF = reflect.ValueOf(lhs).Convert(fType).Float()
		rhsF = reflect.ValueOf(rhs).Convert(fType).Float()
	}
	return lhsF < rhsF
}
