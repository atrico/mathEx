module gitlab.com/atrico/mathEx/v2

go 1.24

require (
	github.com/rs/zerolog v1.33.0
	gitlab.com/atrico/core/v2 v2.2.5
	gitlab.com/atrico/testing/v2 v2.5.3
	go.uber.org/goleak v1.3.0
	golang.org/x/exp v0.0.0-20250218142911-aa4b98e5adaa
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/mattn/go-colorable v0.1.14 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.10.0 // indirect
	golang.org/x/sys v0.30.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
