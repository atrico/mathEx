package mathEx

func FindLowestCommonMultiple(first uint, values ...uint) (lcm uint) {
	// Based on Euclid's algorithm for GCD
	// LCM = a * b / gcd(a,b)
	euclid := func(a, b uint) uint {
		lcm = a * b
		for b > 0 {
			a, b = b, a%b
		}
		return lcm / a
	}
	// Run algorithm for first 2, then run with hcf and third, etc.
	lcm = first
	for _, val := range values {
		lcm = euclid(lcm, val)
	}
	return
}
