package constraints

import (
	"golang.org/x/exp/constraints"
)

type Numeric interface {
	constraints.Integer | constraints.Float
}

type NumericAndComplex interface {
	Numeric | constraints.Complex
}
