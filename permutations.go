package mathEx

import (
	"gitlab.com/atrico/core/v2"
	"iter"
	"math/big"

	"gitlab.com/atrico/core/v2/collection/set"
	"gitlab.com/atrico/core/v2/patterns"
	"gitlab.com/atrico/mathEx/v2/errors"
)

type PermutationSet[T any] interface {
	patterns.Iteratable[[]T]
	Count() *big.Int
}

// ----------------------------------------------------------------------------------------------------------------------------
// Combinations
// ----------------------------------------------------------------------------------------------------------------------------
func Combinations[T any](subsetSize int, population ...T) (combinations PermutationSet[T]) {
	return createSet(population, subsetSize,
		// Calculate count
		func(n, r uint) *big.Int {
			h := n - r
			l := r
			if h < l {
				h, l = l, h
			}
			div := FactorialDiv(n, h)
			return div.Div(div, Factorial(l))
		},
		// Calculate next value
		func(state *[]int, popLen int) bool {
			idx := len(*state) - 1
			for {
				(*state)[idx]++
				if (*state)[idx] < popLen {
					break
				}
				idx--
				if idx < 0 {
					return false
				}
			}
			for i := idx + 1; i < len(*state); i++ {
				(*state)[i] = (*state)[i-1] + 1
			}
			return (*state)[len(*state)-1] < popLen
		})
}

// ----------------------------------------------------------------------------------------------------------------------------
// Permutations
// ----------------------------------------------------------------------------------------------------------------------------
func Permutations[T any](subsetSize int, population ...T) (permutations PermutationSet[T]) {
	return createSet(population, subsetSize,
		// Calculate count
		func(n, r uint) *big.Int {
			return FactorialDiv(n, n-r)
		},
		// Calculate next value
		func(state *[]int, popLen int) bool {
			nextAvailable := func(idx, min int) int {
				used := set.MakeSet[int]((*state)[0:idx]...)
				val := min
				for used.Contains(val) {
					val++
				}
				return val
			}
			idx := len(*state) - 1
			for {
				(*state)[idx] = nextAvailable(idx, (*state)[idx]+1)
				if (*state)[idx] < popLen {
					break
				}
				idx--
				if idx < 0 {
					return false
				}
			}
			for i := idx + 1; i < len(*state); i++ {
				(*state)[i] = nextAvailable(i, 0)
			}

			return (*state)[len(*state)-1] < popLen
		})
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------
type permutationSet[T any] struct {
	count      core.CachedValue[*big.Int]
	population []T
	subsetSize int
	nextFunc   func(state *[]int, popLen int) bool
}

func (p *permutationSet[T]) Count() *big.Int {
	return p.count.GetValue()
}

func (p *permutationSet[T]) Iterator() iter.Seq[[]T] {
	return func(yield func([]T) bool) {
		state := make([]int, p.subsetSize)
		for i := range state {
			state[i] = i
		}
		state[p.subsetSize-1]--
		next := p.iterate(&state)
		for next != nil {
			if !yield(next) {
				break
			}
			next = p.iterate(&state)
		}
	}
}

func createSet[T any](population []T, subsetSize int, countFunc func(n, r uint) *big.Int, nextFunc func(state *[]int, popLen int) bool) (permutations PermutationSet[T]) {
	if len(population) == 0 {
		panic(errors.EmptyPopulation{})
	}
	if len(population) < subsetSize {
		panic(errors.SampleSetTooBig{})
	}
	n := uint(len(population))
	r := uint(subsetSize)
	return &permutationSet[T]{
		core.NewCachedValue(func() *big.Int { return countFunc(n, r) }),
		population,
		subsetSize,
		nextFunc,
	}
}

func (p *permutationSet[T]) iterate(state *[]int) (permutation []T) {
	if p.nextFunc(state, len(p.population)) {
		permutation = make([]T, len(*state))
		for i, idx := range *state {
			permutation[i] = p.population[idx]
		}
	}
	return permutation
}
