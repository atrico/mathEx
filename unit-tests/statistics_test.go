package unit_tests

import (
	"gitlab.com/atrico/core/v2"
	"gitlab.com/atrico/testing/v2/equality"
	constraintsGo "golang.org/x/exp/constraints"
	"reflect"
	"sort"
	"testing"

	"gitlab.com/atrico/mathEx/v2"
	"gitlab.com/atrico/mathEx/v2/constraints"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func init() {
	rg.SetDefaultIntUpto(1000000)
}

func Test_Statistics_Count(t *testing.T) {
	t.Run("int", testComplexImpl(func(p mathEx.PopulationComplex[int]) int { return p.Count() }, func(v []int) int { return len(v) }))
	t.Run("float", testComplexImpl(func(p mathEx.PopulationComplex[float64]) int { return p.Count() }, func(v []float64) int { return len(v) }))
	t.Run("complex", testComplexImpl(func(p mathEx.PopulationComplex[complex64]) int { return p.Count() }, func(v []complex64) int { return len(v) }))
	t.Run("int2", testImpl(func(p mathEx.Population[int]) int { return p.Count() }, func(v []int) int { return len(v) }))
	t.Run("float2", testImpl(func(p mathEx.Population[float64]) int { return p.Count() }, func(v []float64) int { return len(v) }))
}

func Test_Statistics_Values(t *testing.T) {
	t.Run("int", testComplexImpl(func(p mathEx.PopulationComplex[int]) []int { return p.Values() }, func(v []int) []int { return v }))
	t.Run("float", testComplexImpl(func(p mathEx.PopulationComplex[float64]) []float64 { return p.Values() }, func(v []float64) []float64 { return v }))
	t.Run("complex", testComplexImpl(func(p mathEx.PopulationComplex[complex64]) []complex64 { return p.Values() }, func(v []complex64) []complex64 { return v }))
	t.Run("int2", testImpl(func(p mathEx.Population[int]) []int { return p.Values() }, func(v []int) []int { return v }))
	t.Run("float2", testImpl(func(p mathEx.Population[float64]) []float64 { return p.Values() }, func(v []float64) []float64 { return v }))
}

func Test_Statistics_Sum(t *testing.T) {
	t.Run("int", testComplexImpl(func(p mathEx.PopulationComplex[int]) int { return p.Sum() }, calculateSum[int]))
	t.Run("float", testComplexImpl(func(p mathEx.PopulationComplex[float64]) float64 { return p.Sum() }, calculateSum[float64]))
	t.Run("complex", testComplexImpl(func(p mathEx.PopulationComplex[complex64]) complex64 { return p.Sum() }, calculateSum[complex64]))
	t.Run("int2", testImpl(func(p mathEx.Population[int]) int { return p.Sum() }, calculateSum[int]))
	t.Run("float2", testImpl(func(p mathEx.Population[float64]) float64 { return p.Sum() }, calculateSum[float64]))
}

func calculateSum[T constraints.NumericAndComplex](values []T) (result T) {
	for _, v := range values {
		result += v
	}
	return
}

func Test_Statistics_Mean(t *testing.T) {
	t.Run("int", testComplexImpl(func(p mathEx.PopulationComplex[int]) int { return p.Mean() }, func(v []int) int { return calculateMean(v) }))
	t.Run("float", testComplexImpl(func(p mathEx.PopulationComplex[float64]) float64 { return p.Mean() }, func(v []float64) float64 { return calculateMean(v) }))
	t.Run("complex", testComplexImpl(func(p mathEx.PopulationComplex[complex64]) complex64 { return p.Mean() }, func(v []complex64) complex64 { return calculateMean(v) }))
	t.Run("int2", testImpl(func(p mathEx.Population[int]) int { return p.Mean() }, func(v []int) int { return calculateMean(v) }))
	t.Run("float2", testImpl(func(p mathEx.Population[float64]) float64 { return p.Mean() }, func(v []float64) float64 { return calculateMean(v) }))
}

func calculateMean[T constraints.NumericAndComplex](values []T) (result T) {
	return mathEx.Sum(values) / fromInt[T](len(values))
}

func Test_Statistics_MedianOdd(t *testing.T) {
	const popSize = 3
	t.Run("int", testImplCount(func(p mathEx.Population[int]) int { return p.Median() }, func(v []int) int { return calculateMedian(v) }, popSize))
	t.Run("float", testImplCount(func(p mathEx.Population[float64]) float64 { return p.Median() }, func(v []float64) float64 { return calculateMedian(v) }, popSize))
}

func Test_Statistics_MedianEven(t *testing.T) {
	const popSize = 4
	t.Run("int", testImplCount(func(p mathEx.Population[int]) int { return p.Median() }, func(v []int) int { return calculateMedian(v) }, popSize))
	t.Run("float", testImplCount(func(p mathEx.Population[float64]) float64 { return p.Median() }, func(v []float64) float64 { return calculateMedian(v) }, popSize))
}

func calculateMedian[T constraints.Numeric](values []T) (result T) {
	sort.Slice(values, func(i, j int) bool { return values[i] < values[j] })
	if len(values) == 3 {
		return values[1]
	} else {
		return (values[1] + values[2]) / 2
	}
}
func calculateMedianComp[T constraintsGo.Complex](values []T) (result T) {
	sort.Slice(values, func(i, j int) bool { return mathEx.LessComplex(values[i], values[j]) })
	if len(values) == 3 {
		return values[1]
	} else {
		return (values[1] + values[2]) / 2
	}
}

func Test_Statistics_StandardDeviation(t *testing.T) {
	t.Run("int", testComplexImpl(func(p mathEx.PopulationComplex[int]) int { return p.StandardDeviation() }, func(v []int) int { return mathEx.SqrRoot(calculateVariance(v)) }))
	t.Run("float", testComplexImpl(func(p mathEx.PopulationComplex[float64]) float64 { return p.StandardDeviation() }, func(v []float64) float64 { return mathEx.SqrRoot(calculateVariance(v)) }))
	t.Run("complex", testComplexImpl(func(p mathEx.PopulationComplex[complex64]) complex64 { return p.StandardDeviation() }, func(v []complex64) complex64 { return mathEx.SqrRoot(calculateVariance(v)) }))
	t.Run("int2", testImpl(func(p mathEx.Population[int]) int { return p.StandardDeviation() }, func(v []int) int { return mathEx.SqrRoot(calculateVariance(v)) }))
	t.Run("float2", testImpl(func(p mathEx.Population[float64]) float64 { return p.StandardDeviation() }, func(v []float64) float64 { return mathEx.SqrRoot(calculateVariance(v)) }))
}

func Test_Statistics_Variance(t *testing.T) {
	t.Run("int", testComplexImpl(func(p mathEx.PopulationComplex[int]) int { return p.Variance() }, func(v []int) int { return calculateVariance(v) }))
	t.Run("float", testComplexImpl(func(p mathEx.PopulationComplex[float64]) float64 { return p.Variance() }, func(v []float64) float64 { return calculateVariance(v) }))
	t.Run("complex", testComplexImpl(func(p mathEx.PopulationComplex[complex64]) complex64 { return p.Variance() }, func(v []complex64) complex64 { return calculateVariance(v) }))
	t.Run("int2", testImpl(func(p mathEx.Population[int]) int { return p.Variance() }, func(v []int) int { return calculateVariance(v) }))
	t.Run("float2", testImpl(func(p mathEx.Population[float64]) float64 { return p.Variance() }, func(v []float64) float64 { return calculateVariance(v) }))
}

func calculateVariance[T constraints.NumericAndComplex](values []T) (result T) {
	mean := calculateMean(values)
	for _, val := range values {
		result += (val - mean) * (val - mean)
	}
	return result / fromInt[T](len(values))
}

func Test_Statistics_Gini(t *testing.T) {
	t.Run("int", testImpl(func(p mathEx.Population[int]) float64 { return p.GiniImpurity() }, func(v []int) float64 { return calculateGiniI(v) }))
	t.Run("float", testImpl(func(p mathEx.Population[float64]) float64 { return p.GiniImpurity() }, func(v []float64) float64 { return calculateGiniF(v) }))
}

func calculateGiniI(values []int) (result float64) {
	var sum float64
	for _, v := range values {
		sum += float64(v)
	}
	for _, v := range values {
		prob := float64(v) / sum
		result += prob * (1 - prob)
	}
	return
}
func calculateGiniF(values []float64) (result float64) {
	var sum float64
	for _, v := range values {
		sum += v
	}
	for _, v := range values {
		prob := v / sum
		result += prob * (1 - prob)
	}
	return
}

// ----------------------------------------------------------------------------------------------------------------------------
// Helpers
// ----------------------------------------------------------------------------------------------------------------------------

var equalityFloat = equality.Float64(10)
var equalityComplex = equality.Complex64(10)

func testComplexImpl[Tin constraints.NumericAndComplex, Tout any](op func(population mathEx.PopulationComplex[Tin]) Tout, expectFunc func(values []Tin) Tout) func(t *testing.T) {
	return func(t *testing.T) {
		// Arrange
		values := makeValues[Tin](3)
		population, populationM := makePopulationsComplex(values)
		var expected any = expectFunc(values)

		// Act
		result := op(population)
		resultM := op(populationM)

		// Assert
		{
			switch res := any(result).(type) {
			case int:
				assert.That(t, res, is.EqualTo(expected.(int)), "result")
			case float64:
				assert.That(t, res, is.EqualToC(expected.(float64), equalityFloat), "result")
			case complex64:
				assert.That(t, res, is.EqualToC(expected.(complex64), equalityComplex), "result")
			}
		}
		{
			switch res := any(resultM).(type) {
			case int:
				assert.That(t, res, is.EqualTo(expected.(int)), "resultM")
			case float64:
				assert.That(t, res, is.EqualToC(expected.(float64), equalityFloat), "resultM")
			case complex64:
				assert.That(t, res, is.EqualToC(expected.(complex64), equalityComplex), "resultM")
			}
		}
	}
}
func testImpl[Tin constraints.Numeric, Tout any](op func(population mathEx.Population[Tin]) Tout, expectFunc func(values []Tin) Tout) func(t *testing.T) {
	return testImplCount(op, expectFunc, 3)
}
func testImplCount[Tin constraints.Numeric, Tout any](op func(population mathEx.Population[Tin]) Tout, expectFunc func(values []Tin) Tout, count int) func(t *testing.T) {
	return func(t *testing.T) {
		// Arrange
		values := makeValues[Tin](count)
		population, populationM := makePopulations(values)
		var expected any = expectFunc(values)

		// Act
		result := op(population)
		resultM := op(populationM)

		// Assert
		{
			switch res := any(result).(type) {
			case int:
				assert.That(t, res, is.EqualTo(expected.(int)), "result")
			case float64:
				assert.That(t, res, is.EqualToC(expected.(float64), equalityFloat), "result")
			}
		}
		{
			switch res := any(resultM).(type) {
			case int:
				assert.That(t, res, is.EqualTo(expected.(int)), "resultM")
			case float64:
				assert.That(t, res, is.EqualToC(expected.(float64), equalityFloat), "resultM")
			}
		}
	}
}

func makeValues[T constraints.NumericAndComplex](count int) (values []T) {
	values = make([]T, count)
	for i := range values {
		core.MustNotError1(rg.Value, any(&values[i]))
	}
	return
}

func makePopulationsComplex[T constraints.NumericAndComplex](values []T) (populationComplex mathEx.PopulationComplex[T], populationComplexM mathEx.MutablePopulationComplex[T]) {
	populationComplex = mathEx.MakePopulationComplex(values...)
	populationComplexM = mathEx.MakeMutablePopulationComplex[T]()
	for i, value := range values {
		if i > 0 {
			// Cache values (to test cache is reset)
			populationComplexM.Sum()
			populationComplexM.Mean()
			populationComplexM.StandardDeviation()
			populationComplexM.Variance()
		}
		populationComplexM.AddValue(value)
	}
	return
}
func makePopulations[T constraints.Numeric](values []T) (population mathEx.Population[T], populationM mathEx.MutablePopulation[T]) {
	population = mathEx.MakePopulation(values...)
	populationM = mathEx.MakeMutablePopulation[T]()
	for i, value := range values {
		if i > 0 {
			// Cache values (to test cache is reset)
			populationM.Sum()
			populationM.Mean()
			populationM.StandardDeviation()
			populationM.Variance()
			populationM.Median()
			populationM.GiniImpurity()
		}
		populationM.AddValue(value)
	}
	return
}

func fromInt[T constraints.NumericAndComplex](val int) (result T) {
	var rValue reflect.Value
	rType := reflect.TypeOf(result)
	switch rType.Kind() {
	case reflect.Complex64:
		rValue = reflect.ValueOf(complex(float32(val), 0))
	case reflect.Complex128:
		rValue = reflect.ValueOf(complex(float64(val), 0))
	default:
		rValue = reflect.ValueOf(val).Convert(rType)
	}
	reflect.ValueOf(&result).Elem().Set(rValue)
	return result
}
