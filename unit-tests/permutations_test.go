package unit_tests

import (
	"gitlab.com/atrico/core/v2"
	"gitlab.com/atrico/testing/v2/profiling"
	"math/big"
	"reflect"
	"testing"

	"gitlab.com/atrico/mathEx/v2"
	"gitlab.com/atrico/mathEx/v2/errors"
	. "gitlab.com/atrico/testing/v2"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_Permutations(t *testing.T) {
	for name, testCase := range permutationsTestCases {
		t.Run(name, func(t *testing.T) {
			// Act
			permutations := mathEx.Permutations(testCase.size, testCase.population...)
			t.Logf("permutations=%v", permutations)

			// Assert
			assert.That(t, permutations.Count(), is.EqualTo(big.NewInt(int64(len(testCase.expected)))), "count")
			assert.That(t, core.ReadSequenceAsSlice(permutations.Iterator()), is.EqualTo(testCase.expected), "permutations")
		})
	}
}

var permutationsTestCases = map[string]permutationsTestCase{
	"3P3": {3, []int{1, 2, 3}, [][]int{{1, 2, 3}, {1, 3, 2}, {2, 1, 3}, {2, 3, 1}, {3, 1, 2}, {3, 2, 1}}},
	"3P2": {2, []int{1, 2, 3}, [][]int{{1, 2}, {1, 3}, {2, 1}, {2, 3}, {3, 1}, {3, 2}}},
	"5P2": {2, []int{1, 2, 3, 4, 5}, [][]int{{1, 2}, {1, 3}, {1, 4}, {1, 5}, {2, 1}, {2, 3}, {2, 4}, {2, 5}, {3, 1}, {3, 2}, {3, 4}, {3, 5}, {4, 1}, {4, 2}, {4, 3}, {4, 5}, {5, 1}, {5, 2}, {5, 3}, {5, 4}}},
}

type permutationsTestCase struct {
	size       int
	population []int
	expected   [][]int
}

// ----------------------------------------------------------------------------------------------------------------------------
// Errors
// ----------------------------------------------------------------------------------------------------------------------------

func Test_Permutations_EmptyPopulation(t *testing.T) {
	// Arrange

	// Act
	err := PanicCatcher(func() { mathEx.Permutations[string](2) })

	// Assert
	assert.That[any](t, err, is.NotNil, "error")
	assert.That(t, err, is.Type(reflect.TypeOf(errors.EmptyPopulation{})), "error type")
}

func Test_Permutations_SampleSetTooBig(t *testing.T) {
	// Arrange

	// Act
	err := PanicCatcher(func() { mathEx.Permutations(4, "A", "B", "C") })

	// Assert
	assert.That[any](t, err, is.NotNil, "error")
	assert.That(t, err, is.Type(reflect.TypeOf(errors.SampleSetTooBig{})), "error type")
}

// ----------------------------------------------------------------------------------------------------------------------------
// Benchmarks
// ----------------------------------------------------------------------------------------------------------------------------

// Benchmark_Permutations-12    	 4545915	       232.0 ns/op
// Benchmark_Permutations-12    	 4700716	       227.2 ns/op
func Benchmark_Permutations(b *testing.B) {
	stop, _ := profiling.Start(profiling.File("permutations.prof"))
	defer stop()
	for i := 0; i < b.N; i++ {
		mathEx.Permutations(populationSize/2, population...)
	}
}
