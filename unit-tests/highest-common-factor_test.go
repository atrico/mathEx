package unit_tests

import (
	"fmt"
	"gitlab.com/atrico/mathEx/v2"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"testing"
)

func Test_HighestCommonFactor(t *testing.T) {
	for name, testCase := range hcfTestCases {
		t.Run(name, func(t *testing.T) {
			fmt.Println(testCase.values, " =>", testCase.expected)
			// Act
			hcf := mathEx.FindHighestCommonFactor(testCase.values[0], testCase.values[1:]...)

			// Assert
			assert.That(t, hcf, is.EqualTo(testCase.expected), "hcf")
		})
	}
}

// Benchmark_HighestCommonFactor-12    	 1332858	       901.2 ns/op
// Benchmark_HighestCommonFactor-12    	 1361974	       854.5 ns/op
// Benchmark_HighestCommonFactor-12    	 1382737	       904.4 ns/op
// Average                               1359189           886.7
func Benchmark_HighestCommonFactor(b *testing.B) {
	// Values with HCF
	values := []uint{26598050, 15200430, 76060650, 45483960, 15097780, 63722350, 60897200, 41686310, 95111510, 73593950, 74964590, 15195510, 89373410, 40782950, 14227090, 35620970, 48661500, 2180140, 70401180, 83721760, 73406710, 17586800, 47752060, 76127090, 86836290, 5908760, 65031110, 96629410, 20037180, 28590760, 94203720, 5800960, 5016650, 57922710, 88960460, 40698690, 35522520, 58518480, 64043590, 67990030, 17604890, 67782860, 1587590, 82548330, 27600620, 18195080, 27447080, 19716710, 15045740, 2895770}
	// Values with no HCF (HCF = 1)
	valuesNone := []uint{2659805, 1520043, 7606065, 4548396, 1509778, 6372235, 6089720, 4168631, 9511151, 7359395, 7496459, 1519551, 8937341, 4078295, 1422709, 3562097, 4866150, 218014, 7040118, 8372176, 7340671, 1758680, 4775206, 7612709, 8683629, 590876, 6503111, 9662941, 2003718, 2859076, 9420372, 580096, 501665, 5792271, 8896046, 4069869, 3552252, 5851848, 6404359, 6799003, 1760489, 6778286, 158759, 8254833, 2760062, 1819508, 2744708, 1971671, 1504574, 289577}
	for i := 0; i < b.N; i++ {
		mathEx.FindHighestCommonFactor(values[0], values[1:]...)
		mathEx.FindHighestCommonFactor(valuesNone[0], valuesNone[1:]...)
	}
}

var hcfTestCases = map[string]hcfTestCase{
	"2p":        makeHcfTestCase(1, 3, 7),
	"2":         makeHcfTestCase(1, 4, 9),
	"3even":     makeHcfTestCase(2, 4, 6, 12),
	"4":         makeHcfTestCase(3, 3, 6, 12),
	"non prime": makeHcfTestCase(6, 6, 12),
	"big":       makeHcfTestCase(6, 6, 12),
}

type hcfTestCase struct {
	values   []uint
	expected uint
}

func makeHcfTestCase(hcf uint, values ...uint) hcfTestCase {
	return hcfTestCase{values, hcf}
}
