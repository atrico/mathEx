package unit_tests

import (
	"testing"

	"gitlab.com/atrico/mathEx/v2"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/equality"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_SqrRoot(t *testing.T) {
	// Arrange
	i := 16
	f := 25.0
	c1 := complex(rg.Float32(), 0.0)
	c2 := complex(0.0, rg.Float32())
	c3 := rg.Complex128()

	// Act
	rtI := mathEx.SqrRoot(i)
	rtF := mathEx.SqrRoot(f)
	rtC1 := mathEx.SqrRoot(c1)
	rtC2 := mathEx.SqrRoot(c2)
	rtC3 := mathEx.SqrRoot(c3)

	// Assert
	assert.That(t, rtI*rtI, is.EqualTo(i), "int")
	assert.That(t, rtF*rtF, is.EqualToC(f, equality.Float64(3)), "float")
	assert.That(t, rtC1*rtC1, is.EqualToC(c1, equality.Complex64(3)), "complex (real only)")
	assert.That(t, rtC2*rtC2, is.EqualToC(c2, equality.Complex64(3)), "complex (image only)")
	assert.That(t, rtC3*rtC3, is.EqualToC(c3, equality.Complex128(3)), "complex (real only)")
}
