package unit_tests

import (
	"math/big"
	"strconv"
	"testing"

	"gitlab.com/atrico/mathEx/v2"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_Factorials(t *testing.T) {
	for testCase := range factorialsTestCasesTestCases(100) {
		t.Run(strconv.Itoa(int(testCase.value)), func(t *testing.T) {
			// Arrange

			// Act
			result := mathEx.Factorial(testCase.value)

			// Assert
			assert.That(t, result, is.EqualTo(testCase.expected), "comment")
		})
	}
}

func Test_FactorialDiv(t *testing.T) {
	// Arrange
	top := uint(rg.IntBetween(100, 10000))
	bottom := uint(rg.IntBetween(100, 10000))
	if top < bottom {
		top, bottom = bottom, top
	}
	t.Logf("%d / %d", top, bottom)

	// Act
	result := mathEx.FactorialDiv(top, bottom)

	// Assert
	tp := mathEx.Factorial(top)
	expected := tp.Div(tp, mathEx.Factorial(bottom))
	assert.That(t, result, is.EqualTo(expected), "equal")
}

func factorialsTestCasesTestCases(max uint) <-chan factorialsTestCase {
	ch := make(chan factorialsTestCase)
	go func() {
		defer close(ch)
		total := big.NewInt(1)
		for f := int64(0); f < int64(max); f++ {
			if f > 0 {
				total.Mul(total, big.NewInt(f))
			}
			tc := factorialsTestCase{
				value:    uint(f),
				expected: big.NewInt(0),
			}
			tc.expected.Set(total)
			ch <- tc
		}
	}()
	return ch
}

type factorialsTestCase struct {
	value    uint
	expected *big.Int
}
