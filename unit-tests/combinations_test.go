package unit_tests

import (
	"gitlab.com/atrico/core/v2"
	"gitlab.com/atrico/testing/v2/profiling"
	"math/big"
	"reflect"
	"testing"

	"gitlab.com/atrico/mathEx/v2"
	"gitlab.com/atrico/mathEx/v2/errors"
	. "gitlab.com/atrico/testing/v2"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_Combinations(t *testing.T) {
	for name, testCase := range combinationsTestCases {
		t.Run(name, func(t *testing.T) {
			// Act
			combinations := mathEx.Combinations(testCase.size, testCase.population...)
			t.Logf("combinations=%v", combinations)

			// Assert
			assert.That(t, combinations.Count(), is.EqualTo(big.NewInt(int64(len(testCase.expected)))), "count")
			assert.That(t, core.ReadSequenceAsSlice(combinations.Iterator()), is.EqualTo(testCase.expected), "combinations")
		})
	}
}

var combinationsTestCases = map[string]combinationsTestCase{
	"3C3": {3, []int{1, 2, 3}, [][]int{{1, 2, 3}}},
	"3C2": {2, []int{1, 2, 3}, [][]int{{1, 2}, {1, 3}, {2, 3}}},
	"5C2": {2, []int{1, 2, 3, 4, 5}, [][]int{{1, 2}, {1, 3}, {1, 4}, {1, 5}, {2, 3}, {2, 4}, {2, 5}, {3, 4}, {3, 5}, {4, 5}}},
}

type combinationsTestCase struct {
	size       int
	population []int
	expected   [][]int
}

// ----------------------------------------------------------------------------------------------------------------------------
// Errors
// ----------------------------------------------------------------------------------------------------------------------------

func Test_Combinations_EmptyPopulation(t *testing.T) {
	// Arrange

	// Act
	err := PanicCatcher(func() { mathEx.Combinations[string](2) })

	// Assert
	assert.That[any](t, err, is.NotNil, "error")
	assert.That(t, err, is.Type(reflect.TypeOf(errors.EmptyPopulation{})), "error type")
}

func Test_Combinations_SampleSetTooBig(t *testing.T) {
	// Arrange

	// Act
	err := PanicCatcher(func() { mathEx.Combinations(4, "A", "B", "C") })

	// Assert
	assert.That[any](t, err, is.NotNil, "error")
	assert.That(t, err, is.Type(reflect.TypeOf(errors.SampleSetTooBig{})), "error type")
}

// ----------------------------------------------------------------------------------------------------------------------------
// Benchmarks
// ----------------------------------------------------------------------------------------------------------------------------

// Benchmark_Combinations-12    	 5316180	       241.3 ns/op
func Benchmark_Combinations(b *testing.B) {
	stop, _ := profiling.Start(profiling.File("combinations.prof"))
	defer stop()
	for i := 0; i < b.N; i++ {
		mathEx.Combinations(populationSize/2, population...)
	}
}
