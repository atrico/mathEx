package unit_tests

import (
	"fmt"
	"gitlab.com/atrico/mathEx/v2"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"testing"
)

func Test_LowestCommonMultiple(t *testing.T) {
	for name, testCase := range lcmTestCases {
		t.Run(name, func(t *testing.T) {
			fmt.Println(testCase.values, " =>", testCase.expected)
			// Act
			lcm := mathEx.FindLowestCommonMultiple(testCase.values[0], testCase.values[1:]...)

			// Assert
			assert.That(t, lcm, is.EqualTo(testCase.expected), "lcm")
		})
	}
}

// Benchmark_LowestCommonMultiple-12    	  235964	      5279 ns/op
// Benchmark_LowestCommonMultiple-12    	  224778	      4995 ns/op
// Benchmark_LowestCommonMultiple-12    	  182235	      5581 ns/op
// Average                                    214326          5285
func Benchmark_LowestCommonMultiple(b *testing.B) {
	for i := 0; i < b.N; i++ {
		mathEx.FindLowestCommonMultiple(1, lcmValues...)
	}
}

var lcmValues []uint

func init() {
	lcmValues = make([]uint, 100)
	for i := range lcmValues {
		lcmValues[i] = uint(i + 2)
	}
}

var lcmTestCases = map[string]lcmTestCase{
	"2p": makeLcmTestCase(21, 3, 7),
	"2":  makeLcmTestCase(36, 4, 9),
	"3":  makeLcmTestCase(12, 4, 6, 12),
	"4":  makeLcmTestCase(84, 3, 6, 7, 12),
}

func init() {
	// Single
	for i := 0; i < 10; i++ {
		v := uint(rg.IntUpto(1000))
		lcmTestCases[fmt.Sprintf("single %d", v)] = makeLcmTestCase(v, v)
	}
}

type lcmTestCase struct {
	values   []uint
	expected uint
}

func makeLcmTestCase(lcm uint, values ...uint) lcmTestCase {
	return lcmTestCase{values, lcm}
}
