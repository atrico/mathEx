package unit_tests

import (
	"os"
	"testing"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/rs/zerolog/pkgerrors"
	"gitlab.com/atrico/testing/v2/random"
	"go.uber.org/goleak"
)

var rg = random.NewValueGenerator()

func TestMain(m *testing.M) {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stdout})
	zerolog.ErrorStackMarshaler = pkgerrors.MarshalStack
	zerolog.SetGlobalLevel(zerolog.TraceLevel)
	goleak.VerifyTestMain(m)
}

var population []int

const populationSize = 1000000000

func init() {
	population = make([]int, populationSize)
	for i := range population {
		population[i] = i
	}
}
