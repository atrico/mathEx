package unit_tests

import (
	"gitlab.com/atrico/mathEx/v2"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"sort"
	"strconv"
	"testing"
)

func Test_Primes(t *testing.T) {
	for _, testCase := range primesTestCases {
		t.Run(strconv.Itoa(int(testCase.max)), func(t *testing.T) {
			// Act
			primes := mathEx.GeneratePrimes(testCase.max)

			// Assert
			assert.That(t, primes, is.ListEqualTo(testCase.expected), "values")
		})
	}
}

var primesTestCases = []primesTestCase{
	makePrimesTestCase(0),
	makePrimesTestCase(1),
	makePrimesTestCase(2),
	makePrimesTestCase(3, 2),
	makePrimesTestCase(10, 2, 3, 5, 7),
	makePrimesTestCase(100, 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97),
}

type primesTestCase struct {
	max      uint
	expected []uint
}

func makePrimesTestCase(val uint, primes ...uint) primesTestCase {
	return primesTestCase{val, primes}
}

func Test_PrimeFactors(t *testing.T) {
	for _, testCase := range factorsTestCases {
		t.Run(strconv.Itoa(int(testCase.value)), func(t *testing.T) {
			// Act
			factors := mathEx.CalculatePrimeFactors(testCase.value)

			// Assert
			assert.That(t, factors, is.ListEqualTo(testCase.expected), "values")
		})
	}
}

var factorsTestCases = []factorsTestCase{
	makeFactorsTestCase(0),
	makeFactorsTestCase(1),
	makeFactorsTestCase(2, 2),
	makeFactorsTestCase(10, 2, 5),
	makeFactorsTestCase(100, 2, 2, 5, 5),
	makeFactorsTestCase(1000, 2, 2, 2, 5, 5, 5),
	makeFactorsTestCase(1001, 7, 11, 13),
}

type factorsTestCase struct {
	value    uint
	expected []mathEx.ValueWithExponent
}

// Benchmark_Primes-16          321           3730688 ns/op
// Benchmark_Primes-16          315           3667704 ns/op
// Benchmark_Primes-16          321           3599850 ns/op
// Average                      319           3666080
func Benchmark_Primes(b *testing.B) {
	for i := 0; i < b.N; i++ {
		mathEx.GeneratePrimes(1000000)
	}
}

// Wk:Benchmark_PrimeFactors-20    	      25	  47652360 ns/op
// Wk:Benchmark_PrimeFactors-20    	      25	  51567436 ns/op
// Wk:Benchmark_PrimeFactors-20    	      24	  64605046 ns/op
// Wk:Average                             25      54608281
func Benchmark_PrimeFactors(b *testing.B) {
	for i := 0; i < b.N; i++ {
		mathEx.CalculatePrimeFactors(10000000)
	}
}

func makeFactorsTestCase(val uint, factors ...uint) (testCase factorsTestCase) {
	testCase.value = val
	fact := make(map[uint]uint)
	for _, i := range factors {
		fact[i] = fact[i] + 1
	}
	for v, e := range fact {
		testCase.expected = append(testCase.expected, mathEx.ValueWithExponent{Value: v, Exponent: e})
	}
	sort.Slice(testCase.expected, func(i, j int) bool { return testCase.expected[i].Value < testCase.expected[j].Value })
	return
}
