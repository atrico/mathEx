package mathEx

func FindHighestCommonFactor(first uint, values ...uint) (hcf uint) {
	// Euclid's algorithm
	euclid := func(a, b uint) uint {
		for b > 0 {
			a, b = b, a%b
		}
		return a
	}
	// Run algorithm for first 2, then run with hcf and third, etc.
	hcf = first
	for _, val := range values {
		hcf = euclid(hcf, val)
		if hcf == 1 {
			return
		}
	}
	return
}
