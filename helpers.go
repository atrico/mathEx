package mathEx

import (
	constraintsGo "golang.org/x/exp/constraints"
	"math"
	"reflect"

	"gitlab.com/atrico/mathEx/v2/constraints"
)

func Sum[T constraints.NumericAndComplex](values []T) (sum T) {
	for _, value := range values {
		sum += value
	}
	return sum
}

func SqrRoot[T constraints.NumericAndComplex](val T) (result T) {
	var rValue reflect.Value
	rType := reflect.TypeOf(result)
	switch rType.Kind() {
	case reflect.Complex64, reflect.Complex128:
		cVal := reflect.ValueOf(val).Complex()
		var r, i float64
		if imag(cVal) == 0 {
			r = math.Sqrt(real(cVal))
		} else {
			mod := math.Sqrt(real(cVal)*real(cVal) + imag(cVal)*imag(cVal))
			r = math.Sqrt((mod + real(cVal)) / 2)
			i = imag(cVal) / math.Abs(imag(cVal)) * math.Sqrt((mod-real(cVal))/2)
		}
		rValue = reflect.ValueOf(complex(r, i))
	default:
		var fVal float64
		fVal = reflect.ValueOf(val).Convert(reflect.TypeOf(fVal)).Float()
		rValue = reflect.ValueOf(math.Sqrt(fVal))
	}
	reflect.ValueOf(&result).Elem().Set(rValue.Convert(rType))
	return result
}

// Sort complex by increasing modulus
func LessComplex[T constraintsGo.Complex](i, j T) bool {
	var modI, modJ float64
	switch any(i).(type) {
	case complex64:
		modI = float64(real(complex64(i))*real(complex64(i)) + imag(complex64(i))*imag(complex64(i)))
		modJ = float64(real(complex64(j))*real(complex64(j)) + imag(complex64(j))*imag(complex64(j)))
	case complex128:
		modI = real(complex128(i))*real(complex128(i)) + imag(complex128(i))*imag(complex128(i))
		modJ = real(complex128(j))*real(complex128(j)) + imag(complex128(j))*imag(complex128(j))
	}
	return modI < modJ
}
